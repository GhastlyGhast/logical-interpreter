import sys
from enum import Enum

varHash = {'True':True, 'False':False}
inputString = ""

def main():
    if len(sys.argv) > 2:
        print("Too many arguments")
    elif len(sys.argv) == 1:
        s = input('> ')
        while s.strip() != "exit()":
            try:
                run(s.strip())
            except Exception as e:
                print(e)
            s = input('> ' )
    else:
        try:
            with open(sys.argv[1]) as f:
                for l in f:
                    run(l.strip())
        except Exception as e:
            print(e)

def run(s):
    print(intepret(parse(lex(s))))

class TokenType(Enum):
    AND = 0
    OR = 1
    NOT = 2
    VAR = 3
    EQUAL = 4
    IMPLIES = 5
    EQUIV = 6
    LPAREN = 7
    RPAREN = 8

def lex(s):
    start = 0
    current = 0
    toks = []
    def advance(n = 1):
        nonlocal current
        if current + n <= len(s):
            current += n
            return s[current - n:current]
        return '\0'

    def peek(n = 1):
        nonlocal current
        if current + n <= len(s):
            return s[current:current + n]
        return '\0'

    while current < len(s):
        start = current
        c = advance()
        if c == ' ' or c == '\r' or c == '\t':
            pass
        elif c == '(':
            toks.append((TokenType.LPAREN, None))
        elif c == ')':
            toks.append((TokenType.RPAREN, None))
        elif c == '!':
            toks.append((TokenType.NOT, None))
        elif c == '&':
            if advance() == '&':
                toks.append((TokenType.AND,None))
            else:
                raise Exception("Expected second &")
        elif c == '|':
            if advance() == '|':
                toks.append((TokenType.OR,None))
            else:
                raise Exception("Expected second |")
        elif c == '<':
            if advance(2) == '=>':
                toks.append((TokenType.EQUIV,None))
            else:
                raise Exception("Error unexpected character, expected =>")
        elif c == '=':
            if peek() == '>':
                advance()
                toks.append((TokenType.IMPLIES, None))
            else:
                toks.append((TokenType.EQUAL, None))
        elif isAlpha(c):
            while(isAlphaNum(peek())):
                advance()
            toks.append((TokenType.VAR, s[start:current]))
        else:
            raise Exception("Unexpected character")

    return toks

def isAlpha(c):
    return   ord('a') <= ord(c) and ord(c) <= ord('z') \
          or ord('A') <= ord(c) and ord(c) <= ord('Z') \
          or c == '_'

def isNum(c):
    return ord('0') <= ord(c) and ord(c) <= ord('9')

def isAlphaNum(c):
    return isAlpha(c) or isNum(c)

def parse(s):
    res,s = parseAssign(s)
    if s != []:
        raise Exception("Trailing tokens")
    return res

# () <-> V > ! > && > || > => <-> <=> > =
def parseAssign(s):
    l,s = parseImpEq(s)
    if s != [] and s[0][0] == TokenType.EQUAL:
        if l[0] != TokenType.VAR:
            raise Exception("Error : RHS of assignments must be variable")
        r,s = parseImpEq(s[1:])
        l = (TokenType.EQUAL, l, r)
    
    if s != [] and s[0][0] == TokenType.EQUAL:
        raise Exception("Unexpected token, assignments are not associative")
    return (l,s)

def parseImpEq(s):
    l,s = parseOr(s)
    while s != []:
        t = s[0][0]
        if t == TokenType.IMPLIES or t == TokenType.EQUIV:
            r,s = parseOr(s[1:])
            l = (t, l, r) 
        else:
            break

    return (l,s)

def parseOr(s):
    l,s = parseAnd(s)
    while s != []:
        if s[0][0] == TokenType.OR:
            r,s = parseAnd(s[1:])
            l = (TokenType.OR, l, r)
        else:
            break

    return (l,s)

def parseAnd(s):
    l,s = parseNot(s)
    while s != []:
        if s[0][0] == TokenType.AND:
            r,s = parseNot(s[1:])
            l = (TokenType.AND, l, r)
        else:
            break

    return (l,s)

def parseNot(s):
    if s[0][0] == TokenType.NOT:
        r,s = parseNot(s[1:])
        r = (TokenType.NOT, r)
    else:
        r,s = parseVarGroup(s)

    return (r,s)

def parseVarGroup(s):
    if s == []:
        raise Exception("Reached end of file prematurely")
    elif s[0][0] == TokenType.VAR:
        return (s[0],s[1:])
    elif s[0][0] == TokenType.LPAREN:
        res,s = parseAssign(s[1:])
        if s == [] or s[0][0] != TokenType.RPAREN:
            raise Exception("Missing ')'")
        return (res,s[1:])
    else:
        raise Exception("Unexpected character")

def intepret(t):
    if t == ():
        raise Exception("Improper syntax tree")
    elif t[0] == TokenType.VAR:
        if t[1] in varHash.keys():
            return varHash[t[1]]
        raise Exception("Variable not bound")
    elif t[0] == TokenType.EQUAL:
        var = t[1][1]
        varHash[var] = intepret(t[2])
        return varHash[var]
    elif t[0] == TokenType.AND:
        return intepret(t[1]) and intepret(t[2])
    elif t[0] == TokenType.OR:
        return intepret(t[1]) or intepret(t[2])
    elif t[0] == TokenType.IMPLIES:
        return not intepret(t[1]) or intepret(t[2])
    elif t[0] == TokenType.EQUIV:
        l = intepret(t[1])
        r = intepret(t[2])
        return l and r or (not l) and (not r)
    elif t[0] == TokenType.NOT:
        return not intepret(t[1])
    else:
        raise Exception("Unknown token in syntax tree")

if __name__ == "__main__":
    main()
