# Simple Logic Intepreter

This project is a simple interpreter for logical statements written in python, it can be run on a file of expressions or statements separated by newlines or as a REPL supported operators are :
+ '&&' and
+ '||' or
+ '=>' logical implication
+ '<=>' logical equivalence
+ '!' not

Expressions can be grouped inside brackets. Variables must start with a letter or an underscore and be composed of alphanumeric characters and underscores
